def read_answer
  gets.chomp
end

puts "Which DCVS is the best ever?"
the_best_dcvs = read_answer

until the_best_dcvs == 'Git'
	puts "Wrong answer, try again!"
  the_best_dcvs = read_answer
end

puts "Congrats, you're in kata! :)"